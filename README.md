# Charpix #

A console drawing framework used for drawing shapes and content onto a Windows Console screen, written in C#

## Features ##

* Drawing of basic shapes such as **Lines**, **Circles** and **Boxes**.
* Placement of **Text Boxes** and normal Text elements on the screen with option to align text to your choice.
* Functionality for **Input Boxes** which wait for input and return the string.
* Ability to draw **Prompt Boxes** to the screen that wait for input before closing.
* **ColorInfo** class to easily add *Foreground* and *Background* colors to your elements.
* ASCII Image functionality to load ASCII image from text file.


## How to Use ##

After importing the framework, to simply draw an element to the console use:

```
#!c#
// Draw a TextBox
// Usage: TextBox(x, y, width, height, text, padding, border, alignment, textcolor, bordercolor)
Draw.TextBox(2, 3, 30, 3, "This is a TextBox", 2, true, Draw.Alignment.Center, new ColorInfo(ConsoleColor.Black, ConsoleColor.DarkGreen), new ColorInfo(ConsoleColor.DarkGray, ConsoleColor.Black));
```
```
#!c#
// Draw a Line
// Usage: Line(x, y, x2, y2, symbol, startSymbol, endSymbol)
Draw.Line(1, 1, 5, 8, "*", "S", "E");
```
```
#!c#
// Draw Circle
// Usage: Circle(centerX, centerY, radius, symbol, outlineSymbol, fill)
Draw.Circle(5, 4, 8, ".", "#", true);
```