﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Charpix.ASCIITools
{
    public class ASCII
    {
        private static List<ASCIIImage> Images = new List<ASCIIImage>();
        private static string DummyIMG = @"0101010101010101010
0101010101010101010
0101010101010101010
010 PLACEHOLDER 010
0101010101010101010
010101 IMAGE 101010
0101010101010101010
0101010101010101010
0101010101010101010";

        /// <summary>
        /// Grabs all .txt files in a directory and populates the Images list
        /// </summary>
        /// <param name="AddToList">Sets whether to add to current list or clear and create new</param>
        public static bool GetImagesInDirectory(string path, bool AddToList = false)
        {
            if (!Directory.Exists(path))
            {
                Debug.WriteLine(string.Format("[ASCII] GetImagesInDirectory({0}): Directory doesn't exist!", path));
                return false;
            }

            if(!AddToList) Images.Clear();                          // Clear list, if needed

            string[] filePaths = Directory.GetFiles(path, "*.txt"); // Get txt files from directory
            foreach (string file in filePaths)                      // Loop through files, read contents and add to Images list
            {
                string text = System.IO.File.ReadAllText(file);
                ASCIIImage image = new ASCIIImage(Path.GetFileNameWithoutExtension(file), text);
                Images.Add(image);
            }
            return true;
        }
        /// <summary>
        /// Get ASCIIImage from list of Images using name
        /// </summary>
        public static ASCIIImage Get(string name)
        {
            ASCIIImage image = Images.Find(x => x.Name == name);    // Find name in list
            if (image == null)
            {
                Debug.WriteLine(string.Format("[ASCII] Get({0}): This image doesn't exist in the ASCII Images list!", name));
                return new ASCIIImage("dummy", DummyIMG);   // Return dummy
            }
            return image;
        }
        /// <summary>
        /// Get ASCIIImage directly from a text file
        /// </summary>
        public static ASCIIImage Get(string path, string filename)
        {
            string file = Path.Combine(path, filename);
            if (!File.Exists(file))                                                             // Check if file exists
            {
                Debug.WriteLine(string.Format("[ASCII] Get({0}, {1}): File doesn't exist!", path, filename));
                return null;
            }
            string text = System.IO.File.ReadAllText(file);                                     // Read contents of file
            return new ASCIIImage(Path.GetFileNameWithoutExtension(file), text);                // Create ASCIIImage and return
        }
    }
    public class ASCIIImage
    {
        public string Name, Display;
        /// <summary>
        /// Initialises an ASCIIImage object which holds ASCII information
        /// </summary>
        /// <param name="name">Name of image</param>
        /// <param name="display">text that represents the image</param>
        public ASCIIImage(string name, string display)
        {
            this.Name = name;
            this.Display = display;
        }
        /// <summary>
        /// Get Width value
        /// </summary>
        public int Width
        {
            get
            {
                string[] lines = Display.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);
                int max = 0;
                foreach(string line in lines)
                {
                    if (line.Length > max)
                        max = line.Length;
                }
                return max;
            }
        }
        /// <summary>
        /// Get Height value
        /// </summary>
        public int Height
        {
            get
            {
                string[] lines = Display.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);
                return lines.Length;
            }
        }
    }
}
