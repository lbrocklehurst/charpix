﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Charpix
{
    public class DrawTools
    {
        /// <summary>
        /// Returns an array of Coords representative of a line from (x, y) to (x2, y2)
        /// </summary>
        public static Coord[] Line(int x, int y, int x2, int y2)
        {
            List<Coord> coords = new List<Coord>();
            int w = x2 - x;
            int h = y2 - y;
            int dx1 = 0, dy1 = 0, dx2 = 0, dy2 = 0;
            if (w < 0) dx1 = -1; else if (w > 0) dx1 = 1;
            if (h < 0) dy1 = -1; else if (h > 0) dy1 = 1;
            if (w < 0) dx2 = -1; else if (w > 0) dx2 = 1;
            int longest = Math.Abs(w);
            int shortest = Math.Abs(h);
            if (!(longest > shortest))
            {
                longest = Math.Abs(h);
                shortest = Math.Abs(w);
                if (h < 0) dy2 = -1; else if (h > 0) dy2 = 1;
                dx2 = 0;
            }
            int numerator = longest >> 1;
            for (int i = 0; i <= longest; i++)
            {
                coords.Add(new Coord(x, y));
                numerator += shortest;
                if (!(numerator < longest))
                {
                    numerator -= longest;
                    x += dx1;
                    y += dy1;
                }
                else
                {
                    x += dx2;
                    y += dy2;
                }
            }
            return coords.ToArray();
        }

        public static bool OutOfBounds(int x, int y)
        {
            if(x >= Console.WindowWidth
                || x < 0
                || y >= Console.WindowHeight
                || y < 0)
                return true;
            return false;
        }
    }
}
