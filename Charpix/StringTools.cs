﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Charpix
{
    public class StringTools
    {
        /// <summary>
        /// Splits the input string into chunks of a specific size and returns an array of chunks
        /// </summary>
        public static string[] SplitIntoChunks(string input, int chunkSize)
        {
            if (chunkSize <= 0) // Chunk size shouldn't be 0 or under
                chunkSize = 1;
            List<string> Chunks = new List<string>();
            int stringLength = input.Length;    // Get the length of input string

            int startPos = 0;   // Set pointer position
            for (int i = 0; i < stringLength; i += startPos)
            {
                string chunk = input.Substring(i, stringLength - i).CropWholeWords(chunkSize);  // Get the whole chunk and then use CropWholeWords() to not cut off words
                Chunks.Add(chunk);  // Add to list
                startPos = chunk.Length;    // Set new pointer position based on chunk's length (could be smaller than chunkSize)
            }
            return Chunks.ToArray();
        }
        /// <summary>
        /// Find first word from input text
        /// </summary>
        public static string FirstWord(string text)
        {
            if (text == null) throw new ArgumentNullException(text);

            StringBuilder builder = new StringBuilder();

            for (int index = 0; index < text.Length; index += 1)
            {
                char ch = text[index];
                if (Char.IsWhiteSpace(ch)) break;

                builder.Append(ch);
            }

            return builder.ToString();
        }
        /// <summary>
        /// Function wrapper for TextInfo.ToTitleCase
        /// </summary>
        public static string ToTitleCase(string input)
        {
            return CultureInfo.CurrentCulture.TextInfo.ToTitleCase(input.ToLower());
        }

        /// <summary>
        /// Get string sequence representing a progress bar
        /// </summary>
        /// <param name="value">Current value</param>
        /// <param name="max">Maximum value</param>
        /// <param name="Length">Desired length of string</param>
        public static string GetProgressBar(int value, int max, int Length)
        {
            float Perc = (float)value / max;
            int Amount = (int)Math.Round(Perc * Length);
            int Remaining = Length - Amount;
            return new String(Chars.Block[0], Amount) + new String(Chars.LightDither[0], Remaining);
        }
    }
}
