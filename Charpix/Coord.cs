﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Charpix
{
    public class Coord
    {
        public int x = 0;
        public int y = 0;
        /// <summary>
        /// Initialises a Coord object which stores an x and y value
        /// </summary>
        public Coord() { }
        public Coord(int x, int y) { this.x = x; this.y = y; }
        public Coord(Coord coord) { this.x = coord.x; this.y = coord.y; }
    }
}
