﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Charpix
{
    public class Draw
    {
        public enum Alignment
        {
            TopLeft,
            TopCenter,
            TopRight,
            Left,
            Center,
            Right,
            BottomLeft,
            BottomCenter,
            BottomRight
        }
        public enum PromptText
        {
            None,
            [StringValue("Press any key to continue...")]
            AnyKey,
            [StringValue("[ENTER]")]
            Enter
        }
        /// <summary>
        /// Draws a TextBox
        /// </summary>
        /// <param name="padding">Amount of padding around text</param>
        /// <param name="align">Alignment of text</param>
        /// <returns>Returns height of TextBox</returns>
        public static int TextBox(int x, int y, int width, int height, string text,
            int padding = 0, bool border = true, Alignment align = Alignment.Center,
            ColorInfo textcolor = null, ColorInfo bordercolor = null)
        {
            if (DrawTools.OutOfBounds(x, y))
            {
                Debug.WriteLine(string.Format("{0}, {1} is Out of Bounds for {2}!", x, y, System.Reflection.MethodBase.GetCurrentMethod().Name));
                return 0;
            }
            Coord oldCursorPos = new Coord(Console.CursorLeft, Console.CursorTop);  // Store current cursor position
            Console.SetCursorPosition(x, y);    // Set cursor position to first point

            if (text == null || text == "")
                return 0; // No text, no box - no height.

            if (width == 1 && height == 1)       // Tiny box - don't bother drawing anything else.
            {
                Box(x, y, width, height, border);
                return 1;
            }

            int insideWidth = width - 2,
                insideHeight = height - 2;  // Calculate actual inside height and width (inside border)

            //
            // LINECOUNT - Work out lines of text and get linecount
            //
            int linecount = 0;
            List<string> linesToPrint = new List<string>();
            string[] lines = text.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);    // Split linebreaks
            for (int i = 0; i < lines.Length; i++)                                                  // Loop through linebreak array
            {
                if (lines[i] == "")
                    continue;
                if (lines[i].Length > insideWidth - (padding * 2))      // Current line is longer than the inside width and padding
                {
                    string[] split = StringTools.SplitIntoChunks(lines[i], insideWidth - (padding * 2));    // Cut this line up using SplitIntoChunks()
                    linesToPrint.AddRange(split);                                                           // Add generated array to list
                    linecount += split.Length;                                                              // Add to actual linecount
                }
                else
                {
                    linesToPrint.Add(lines[i]);                                                             // Line is not longer than inside width
                    linecount += 1;                                                                         // Add 1 to linecount
                }
            }
            int heightWithText = linecount + 2 + (padding * 2);             // Add padding top and bottom + border to linecount
            height = (height < heightWithText) ? heightWithText : height;   // Check if height is still larger than calculated height
            insideHeight = height - 2;                                      // Update new height values

            // DRAW BOX
            Box(x, y, width, height, border, (textcolor != null) ? textcolor.Background : ConsoleColor.Black, bordercolor);

            // DRAW LINES OF TEXT
            for (int l = 0; l < linesToPrint.Count; l++)
            {
                int startX = 0;
                int startY = 0;
                // ALIGNMENT X
                switch(align)   // Check alignment and update x start position
                {
                    case Alignment.Left: case Alignment.TopLeft: case Alignment.BottomLeft:
                        startX = x + 1 + padding;                                                   // On left, just add padding
                        break;
                    case Alignment.Center: case Alignment.TopCenter: case Alignment.BottomCenter:
                        startX = x + 1 + CentrePaddingLeft(linesToPrint[l].Length, insideWidth);    // CentrePaddingLeft calculates the correct spacing to centre the text
                        break;
                    case Alignment.Right: case Alignment.TopRight: case Alignment.BottomRight:
                        startX = x + insideWidth - linesToPrint[l].Length;                          // Find distance from the right
                        break;
                }
                // ALIGNMENT Y
                switch (align)   // Check alignment and update y start position
                {
                    case Alignment.TopLeft: case Alignment.TopCenter: case Alignment.TopRight:
                        startY = y + 1 + padding;                                       // At top, just add padding
                        break;
                    case Alignment.Left: case Alignment.Center: case Alignment.Right:
                        startY = y + CentrePaddingLeft(linecount, insideHeight) + 1;    // CentrePaddingLeft calculates the correct spacing to centre the text
                        break;
                    case Alignment.BottomLeft: case Alignment.BottomCenter: case Alignment.BottomRight:
                        startY = y + insideHeight - linecount;                          // Find distance from the bottom
                        break;
                }

                Text(startX, startY + l, linesToPrint[l], textcolor);   // Draw text
            }

            Console.SetCursorPosition(oldCursorPos.x, oldCursorPos.y);  // Move cursor back to old position
            return height;  // Return the height (could be higher/lower than input)
        }

        /// <summary>
        /// Draw a Box
        /// </summary>
        public static void Box(int x, int y, int width, int height, bool border = false,
            ConsoleColor fillcolor = ConsoleColor.Black, ColorInfo bordercolor = null)
        {
            if (DrawTools.OutOfBounds(x, y))
            {
                Debug.WriteLine(string.Format("{0}, {1} is Out of Bounds for {2}!", x, y, System.Reflection.MethodBase.GetCurrentMethod().Name));
                return;
            }
            if (width == 1 || height == 1)  // Box is too small, no need for a border
                border = false;

            Coord oldCursorPos = new Coord(Console.CursorLeft, Console.CursorTop);  // Store current cursor position
            Console.SetCursorPosition(x, y);                                        // Set cursor position to first point

            // FILL BOX
            for(int h = 0; h < height; h++) // Loop through the height and fill with BG color
            {
                Console.BackgroundColor = fillcolor;
                Text(x, y + h, new string(' ', width));
                Console.ResetColor();
            }

            if(!border) // No border, end here.
                return;

            if (bordercolor != null)    // Set border colors if applicable
            {
                Console.ForegroundColor = bordercolor.Foreground;
                Console.BackgroundColor = bordercolor.Background;
            }

            Row(x, y, width, "═", "╔", "╗");                    // Draw top row with corners (border)
            Row(x, y + (height - 1), width, "═", "╚", "╝");     // Draw bottom row with corners (border)

            if (height - 2 > 0) // Check if the box needs sides
            {
                Line(x, y + 1, x, y + height - 2, "║");                             // Draw left side border
                Line(x + width - 1, y + 1, x + width - 1, y + height - 2, "║");     // Draw right side border
            }

            Console.ResetColor();                                                   // Reset colors
            Console.SetCursorPosition(oldCursorPos.x, oldCursorPos.y);              // Reset cursor position
        }
        /// <summary>
        /// Draw a Row of 'symbol' with start and end symbols if required
        /// </summary>
        /// <param name="symbol">Symbol to draw</param>
        /// <param name="start">Start symbol</param>
        /// <param name="end">End symbol</param>
        public static void Row(int x, int y, int length, string symbol, string start = "", string end = "")
        {
            if (DrawTools.OutOfBounds(x, y))
            {
                Debug.WriteLine(string.Format("{0}, {1} is Out of Bounds for {2}!", x, y, System.Reflection.MethodBase.GetCurrentMethod().Name));
                return;
            }
            string line = "";

            Coord oldCursorPos = new Coord(Console.CursorLeft, Console.CursorTop);  // Store current cursor position
            Console.SetCursorPosition(x, y);                                        // Set cursor position to first point

            line += (start != "") ? start : symbol; // Add start symbol

            for (int i = 0; i < length - 2; i++)    // Loop through length and place symbol
                line += symbol;
            line += (end != "") ? end : symbol;     // Add end symbol

            Console.WriteLine(line);                                    // Write row of characters at position
            Console.SetCursorPosition(oldCursorPos.x, oldCursorPos.y);  // Reset cursor position
        }
        /// <summary>
        /// Draw a line from x,y to x2,y2
        /// </summary>
        /// <param name="symbol">Symbol to draw</param>
        /// <param name="start">Start symbol</param>
        /// <param name="end">End symbol</param>
        public static void Line(int x, int y, int x2, int y2, string symbol, string start = "", string end = "")
        {
            if (DrawTools.OutOfBounds(x, y))
            {
                Debug.WriteLine(string.Format("{0}, {1} is Out of Bounds for {2}!", x, y, System.Reflection.MethodBase.GetCurrentMethod().Name));
                return;
            }
            Coord[] coords = DrawTools.Line(x, y, x2, y2);  // Fetch an array of points representing the line
            for (int i = 0; i <= coords.Length - 1; i++)    // Loop through the array of Coords
            {
                if(i == 0)
                    Text(coords[i].x, coords[i].y, (start != "") ? start : symbol); // If first, draw start symbol
                else if (i == coords.Length - 1)
                    Text(coords[i].x, coords[i].y, (end != "") ? end : symbol);     // If last, draw end symbol
                else
                    Text(coords[i].x, coords[i].y, symbol);                         // Else, draw middle symbol
            }
        }
        /// <summary>
        /// Draw a circle
        /// </summary>
        /// <param name="symbol">Symbol to draw</param>
        /// <param name="outline">Outline symbol</param>
        /// <param name="fill">Fill in circle</param>
        public static void Circle(int centerX, int centerY, int radius, string symbol, string outline = "", bool fill = true)
        {
            double dDensity = 1.0;  // Set the density of points
            double y, x;
            for (x = -radius; x <= radius; x += dDensity)   // Work through from -radius to radius, incrementing density
            {
                y = Math.Sqrt(-Math.Pow(x, 2.0f) + Math.Pow(radius, 2.0f)); // Calculate the y position from Sqrt(x^2 + radius^2)

                if(fill)    // If circle should be filled, draw a line from the point to the centre
                    Line((int)x + centerX, (int)y + centerY, (int)x + centerX, centerY, symbol, (outline != "") ? outline : symbol, symbol);
                else        // If not, just draw the point
                    Text((int)x + centerX, (int)y + centerY, symbol);

                y = -y;     // Flip the y and repeat the steps
                if (fill)
                    Line((int)x + centerX, (int)y + centerY, (int)x + centerX, centerY, symbol, (outline != "") ? outline : symbol, symbol);
                else
                    Text((int)x + centerX, (int)y + centerY, symbol);
            }
        }
        /// <summary>
        /// Draw Text at point on screen
        /// </summary>
        public static void Text(int x, int y, string text)
        {
            if (DrawTools.OutOfBounds(x, y))
            {
                Debug.WriteLine(string.Format("{0}, {1} is Out of Bounds for {2}!", x, y, System.Reflection.MethodBase.GetCurrentMethod().Name));
                return;
            }
            Coord oldCursorPos = new Coord(Console.CursorLeft, Console.CursorTop);  // Store cursor position
            Console.SetCursorPosition(x, y);                                        // Set cursor position to point
            Console.WriteLine(text);                                                // Draw text
            Console.SetCursorPosition(oldCursorPos.x, oldCursorPos.y);              // Reset cursor
        }
        /// <summary>
        /// Draw text on screen with Color
        /// </summary>
        public static void Text(int x, int y, string text, ConsoleColor fg, ConsoleColor bg)
        {
            if (DrawTools.OutOfBounds(x, y))
            {
                Debug.WriteLine(string.Format("{0}, {1} is Out of Bounds for {2}!", x, y, System.Reflection.MethodBase.GetCurrentMethod().Name));
                return;
            }
            Coord oldCursorPos = new Coord(Console.CursorLeft, Console.CursorTop);
            Console.SetCursorPosition(x, y);
            Console.ForegroundColor = fg;
            Console.BackgroundColor = bg;
            Console.WriteLine(text);
            Console.ResetColor();
            Console.SetCursorPosition(oldCursorPos.x, oldCursorPos.y);
        }
        /// <summary>
        /// Draw text on screen with Color
        /// </summary>
        public static void Text(int x, int y, string text, ColorInfo textcolor)
        {
            if (textcolor == null)  // If provided ColorInfo is null, use no color
            {
                Text(x, y, text);
                return;
            }
            if (DrawTools.OutOfBounds(x, y))
            {
                Debug.WriteLine(string.Format("{0}, {1} is Out of Bounds for {2}!", x, y, System.Reflection.MethodBase.GetCurrentMethod().Name));
                return;
            }
            Coord oldCursorPos = new Coord(Console.CursorLeft, Console.CursorTop);
            Console.SetCursorPosition(x, y);
            Console.ForegroundColor = textcolor.Foreground;
            Console.BackgroundColor = textcolor.Background;
            Console.WriteLine(text);
            Console.ResetColor();
            Console.SetCursorPosition(oldCursorPos.x, oldCursorPos.y);
        }
        /// <summary>
        /// Draw a line of text that disappears upon pressing Enter
        /// </summary>
        /// <param name="prompttext">PromptText type to use</param>
        public static void Prompt(int x, int y, string text, PromptText prompttext = PromptText.AnyKey)
        {
            if (DrawTools.OutOfBounds(x, y))
            {
                Debug.WriteLine(string.Format("{0}, {1} is Out of Bounds for {2}!", x, y, System.Reflection.MethodBase.GetCurrentMethod().Name));
                return;
            }
            Text(x, y, text);                                                       // Draw line of text
            if(prompttext != PromptText.None)
                Text(x, y + 1, StringEnum.GetStringValue(prompttext));              // Draw prompt text (unless set to None)
            Console.ReadLine();                                                     // Wait for input
            ClearRow(x, y, text.Length);                                            // Clear the row of text
            if (prompttext != PromptText.None)
                ClearRow(x, y + 1, StringEnum.GetStringValue(prompttext).Length);   // Clear the prompt row of text
        }
        /// <summary>
        /// Draw a TextBox prompt that disappears upon pressing Enter
        /// </summary>
        /// <param name="padding">Amount of padding around text</param>
        /// <param name="align">Alignment of text</param>
        /// <param name="prompttext">PromptText type to use</param>
        public static void PromptBox(int x, int y, int width, int height, string text,
            int padding = 0, bool border = true, Alignment align = Alignment.Center,
            PromptText prompttext = PromptText.AnyKey,
            ColorInfo textcolor = null, ColorInfo bordercolor = null)
        {
            if (DrawTools.OutOfBounds(x, y))
            {
                Debug.WriteLine(string.Format("{0}, {1} is Out of Bounds for {2}!", x, y, System.Reflection.MethodBase.GetCurrentMethod().Name));
                return;
            }
            string prompt = (prompttext != PromptText.None) ? "\n" + StringEnum.GetStringValue(prompttext) : "";   // Get prompt text
            string newText = text + prompt;                           // Merge text and prompt text together
            TextBox(x, y, width, height, newText, padding, border, align, textcolor, bordercolor);          // Draw TextBox
            Console.ReadLine();                                                                             // Wait for input
            Box(x, y, width, height, false);                                                                // Draw blank box over top
        }
        /// <summary>
        /// Draw a line of text that waits for user input and returns it
        /// </summary>
        /// <param name="text">Text to display above input line, if any</param>
        /// <returns>Returns the input from user</returns>
        public static string Input(int x, int y, string text = "")
        {
            if (DrawTools.OutOfBounds(x, y))
            {
                Debug.WriteLine(string.Format("{0}, {1} is Out of Bounds for {2}!", x, y, System.Reflection.MethodBase.GetCurrentMethod().Name));
                return "";
            }
            Coord oldCursorPos = new Coord(Console.CursorLeft, Console.CursorTop);  // Store cursor position
            if (text != "")                                                         // Check if title text is needed
                Text(x, y, text);                                                   // Draw line of text above input, if needed
            Console.SetCursorPosition(x, y + ((text != "") ? 1 : 0));               // Move cursor to position
            string result = Console.ReadLine();                                     // Read the line from user
            Console.SetCursorPosition(oldCursorPos.x, oldCursorPos.y);              // Reset cursor
            return result;                                                          // Return input
        }
        /// <summary>
        /// Draw a TextBox that waits for user input and returns it
        /// </summary>
        /// <param name="padding">Amount of padding around text</param>
        /// <param name="align">Alignment of text</param>
        /// <returns>Returns the input from user</returns>
        public static string InputBox(int x, int y, int width, int height, string text = "",
            int padding = 0, bool border = true, Alignment align = Alignment.Center,
            ColorInfo textcolor = null, ColorInfo bordercolor = null)
        {
            if (DrawTools.OutOfBounds(x, y))
            {
                Debug.WriteLine(string.Format("{0}, {1} is Out of Bounds for {2}!", x, y, System.Reflection.MethodBase.GetCurrentMethod().Name));
                return "";
            }
            Coord oldCursorPos = new Coord(Console.CursorLeft, Console.CursorTop);
            if (text != "") text += "\n ";                                                                       // If text is needed, add a linebreak
            int boxheight = TextBox(x, y, width, height, text, padding, border, align, textcolor, bordercolor); // Draw TextBox
            Console.SetCursorPosition(x + padding + 1, y + (boxheight - 2 - padding));                          // Move cursor to position for input
            string result = Console.ReadLine();                                                                 // Read the line from user
            Box(x, y, width, height, false);                                                                    // Draw blank box over top
            Console.SetCursorPosition(oldCursorPos.x, oldCursorPos.y);
            return result;
        }
        /// <summary>
        /// Clear a row of text at x,y with desired length
        /// </summary>
        public static void ClearRow(int x, int y, int length)
        {
            if (DrawTools.OutOfBounds(x, y))
            {
                Debug.WriteLine(string.Format("{0}, {1} is Out of Bounds for {2}!", x, y, System.Reflection.MethodBase.GetCurrentMethod().Name));
                return;
            }
            Coord oldCursorPos = new Coord(Console.CursorLeft, Console.CursorTop);
            Console.SetCursorPosition(x, y);
            Console.WriteLine(new string(' ', length));
            Console.SetCursorPosition(oldCursorPos.x, oldCursorPos.y);
        }
        /// <summary>
        /// Calculate left padding to centre text
        /// </summary>
        private static int CentrePaddingLeft(int stringLength, int space) { return (int)Math.Floor(CentrePadding(stringLength, space)); }
        /// <summary>
        /// Calculate right padding to centre text
        /// </summary>
        private static int CentrePaddingRight(int stringLength, int space) { return (int)Math.Ceiling(CentrePadding(stringLength, space)); }
        /// <summary>
        /// Calculate padding amount
        /// </summary>
        private static float CentrePadding(int stringLength, int space) { return (float)(space - stringLength) / 2; }
    }
    public class ColorInfo
    {
        public ConsoleColor Foreground, Background;
        /// <summary>
        /// Initialises a ColorInfo class which stores a Foreground and Background ConsoleColor
        /// </summary>
        /// <param name="fg">Foreground color</param>
        /// <param name="bg">Background color</param>
        public ColorInfo(ConsoleColor fg = ConsoleColor.Gray, ConsoleColor bg = ConsoleColor.Black)
        {
            Foreground = fg;
            Background = bg;
        }
    }
}
