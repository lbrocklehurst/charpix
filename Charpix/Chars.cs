﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Charpix
{
    public class Chars
    {
        public static string LightDither = "░", Dither = "▒", DarkDither = "▓", Block = "█",
            TopLeft = "╔", TopRight = "╗", BottomLeft = "╚", BottomRight = "╝", HorizLine = "═", VertLine = "║";
    }
}
